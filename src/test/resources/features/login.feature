Feature: login

  @smoke
  Scenario: search fruit in wiki page
    Given i have logged into wikiPage
    And i validated following links appear in the page
      | English  |
      | Español  |
      | Italiano |
      | Deutsch  |
      | Polski   |
    When i search for 'apple' using search bar
    Then i should be navigated to "apple" page
    
     @smoke
  Scenario: search fruit in wiki page for orange
    Given i have logged into wikiPage
    And i validated following links appear in the page
      | English  |
      | Español  |
      | Italiano |
      | Deutsch  |
      | Polski   |
    When i search for 'orange' using search bar
    Then i should be navigated to "orange" page
package com.adr.cucumber.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.steps.ScenarioSteps;

public class GenUtil extends ScenarioSteps{
	private static final long serialVersionUID = 1L;

	/*
	 *  it will return total number of elements from the list
	 */
	public static int getTotalCount(List<WebElementFacade> listElements, int count) {

		if (!listElements.isEmpty()) {
			for (WebElementFacade e : listElements) {

				count = count + 1;

			}
		}

		return count;

	}
	
	/*
	 * validate text doesn't exist in list of the element
	 */
	
	
	public static void validateTextDoesNotExistsInTheList(List<String> listElements, String text){
		Boolean found = false;
		for (String e : listElements) {

			if (e.contains(text)) {
				found = true;
				break;
			}

		}
		assertThat(found).overridingErrorMessage( text+ " is present in the list which was not expected").isFalse();
		
	}
	
	
	/* 
	 * validate text  exists in the list of elements
	 */
	
	public static void validateTextExistsInTheList(List<String> listElements, String text){
		Boolean found = false;
		for (String e : listElements) {

			if (e.contains(text)) {
				found = true;
				break;
			}

		}
		assertThat(found).overridingErrorMessage(text+ " is not presnt in list of the elements").isTrue();
		
	}
		
	
	
	/*
	 * Validate that text compared are equal
	 */
	
	public static void validateTextsAreEqual(String expectedText, String textFromPage){
		assertThat(expectedText).isEqualToIgnoringCase(textFromPage);
	
	}
	
	/*
	 * Validates all the expected test passed from feature file is present
	 */
	
	public void validateAllLinksPresent(List<List<String>> data) {

		System.out.println("Link size is: " + data.size());
		for (int i = 0; i < data.size(); i++) {
			assertThat(getDriver().findElement(By.partialLinkText(data.get(i).get(0))).isDisplayed());

		}
	
	}
}

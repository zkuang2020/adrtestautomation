package com.adr.cucumber.util;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


public class CssUtil extends PageObject {
	
	public static void waitForTextToAppear(WebDriver driver, String textToAppear, WebElementFacade element) {
	    WebDriverWait wait = new WebDriverWait(driver,30);
	    wait.until(ExpectedConditions.textToBePresentInElement(element, textToAppear));
	}
	
	

	public static void waitForPageToAppear(WebDriver driver, WebElementFacade elementName){
		WebDriverWait wait = new WebDriverWait(driver,20);
		 wait.until(ExpectedConditions.visibilityOf(elementName));

	}
	
	
		

}

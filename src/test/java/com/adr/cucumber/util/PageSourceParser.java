package com.adr.cucumber.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import net.serenitybdd.core.annotations.findby.By;

public class PageSourceParser {
	private List<String> textFromAllElements;
	private String cssSelector;
	private Document entireSource;
	private Elements allJsoupElements;

	public PageSourceParser(WebDriver driver) {
		entireSource = Jsoup.parse(driver.getPageSource());
	}

	public PageSourceParser(String cssSelectorForElements, WebDriver driver) {

		textFromAllElements = new ArrayList<String>();
		cssSelector = cssSelectorForElements;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(cssSelector)));

		entireSource = Jsoup.parse(driver.getPageSource());
		allJsoupElements = entireSource.select(cssSelector);

	}

	/**
	 * Return true if the list contains the given string
	 * 
	 * @param searchString
	 * @return boolean
	 */
	public boolean listContainsText(String searchString) {
		Iterator<String> it = textFromAllElements.iterator();
		while (it.hasNext()) {
			if (it.next().contains(searchString)) {
				return true;

			}

		}
		return false;
	}

	/**
	 * Return all visible text from the objects that match the given CSS
	 * Selector
	 */

	public List<String> getListOfTextFromAllElements() {
		populateTextFromAllElements();
		return textFromAllElements;
	}

	/**
	 * Get all the visible text from the objects that match the given css
	 * Selector, one simple string
	 * 
	 * @return
	 */

	public String getAllTextFromMatchingElements() {
		populateTextFromAllElements();
		String allText = "";
		for (String a : textFromAllElements)
			allText = allText + a + "\n";
		return allText;
	}

	private void populateTextFromAllElements() {

		Iterator<Element> it = allJsoupElements.iterator();
		textFromAllElements = new ArrayList<String>();
		while (it.hasNext()) {
			Element current = it.next();
			if (current.text() != null)
				textFromAllElements.add(current.text());
			else
				textFromAllElements.add("noTextInElement");
		}

	}

	public String getCssSelector() {
		return cssSelector;
	}

	/**
	 * if the source has changed since the last time it was loaded,,
	 * 
	 * @param driver
	 */

	public void refreshEntireSource(WebDriver driver) {
		entireSource = Jsoup.parse(driver.getPageSource());
	}

	/**
	 * returns the entire source
	 */
	public String getAllTextFromSource() {
		return entireSource.text();
	}

}

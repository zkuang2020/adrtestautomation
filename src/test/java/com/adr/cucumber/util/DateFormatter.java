package com.adr.cucumber.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {

	
	public static String getCurrentDate(String formatDate){
	
	DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	Date date = new Date();
	return dateFormat.format(date);

}
}

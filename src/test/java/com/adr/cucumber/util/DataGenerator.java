package com.adr.cucumber.util;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Random;

public class DataGenerator {
	
	
	/**
	 * @returns delete reason  from file
	 * @throws IOException
	 */
	
	public static String randomDeleteReason(){
		Random rand = new Random();
		String returnValue="";
		try
		{
			RandomAccessFile raf = new RandomAccessFile(System.getProperty("user.dir") + "/src/test/java/gov/uspto/corporate/util/DeleteReason.txt", "r");
			final long randomLocation = (long) rand.nextInt((((int) (raf.length())-20)-0) +1) +0;
			//final long randomLocation = (long) rand.nextInt((int) (raf.length()));
			raf.seek(randomLocation);
			raf.readLine();
			returnValue=raf.readLine().trim();
			raf.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		return returnValue;
	}

}

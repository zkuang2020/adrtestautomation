package com.adr.cucumber.maps;

import java.util.List;

import com.adr.cucumber.user.WikiUser;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import cucumber.api.java.en.Then;


public class wikiMap {
	
	@Steps WikiUser wikiSteps;
	
	@Given("i have logged into wikiPage")
	public void loggedIntoWikiPage(){
		
		wikiSteps.openWikiPage();
		
	}
	
	@Given("^i validated following links appear in the page$")
	public void i_validated_following_links_appear_in_the_page(DataTable link)  {
		List<List<String>> data = link.raw();
		wikiSteps.validateAllLinksPresent(data);
	   
	}

	@When("i search for '(.*)' using search bar")
	public void search(String fruit)
	{
		wikiSteps.searchForFruit(fruit);
	}
	
	@Then("^i should be navigated to \"([^\"]*)\" page$")
	public void i_should_be_navigated_to_page(String text) throws Throwable {
	    wikiSteps.verifyThePage(text);
	}


	
	

}

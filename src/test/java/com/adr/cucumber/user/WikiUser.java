package com.adr.cucumber.user;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.openqa.selenium.By;

import com.adr.cucumber.pages.WikiPage;
import com.adr.cucumber.util.EnvironmentCleanUp;
import com.adr.cucumber.util.GenUtil;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class WikiUser extends ScenarioSteps {
	
	private static final long serialVersionUID = 1L;
	private WikiPage wikiPage;
	private GenUtil genUtil;
	
	@Step
	public void openWikiPage() {
		//EnvironmentCleanUp.clearSession();
		wikiPage.open();
		
	}

	@Step
	public void searchForFruit(String fruit) {
		wikiPage.enterFruitNameInSearchBar(fruit);
		wikiPage.clickSearchBar();
		
	}

	@Step
	public void verifyThePage(String expectedText) {
		String titleFromPage = wikiPage.getPageTitle();
		GenUtil.validateTextsAreEqual(expectedText, titleFromPage);
		
	}

	public void validateAllLinksPresent(List<List<String>> links) {
		System.out.println("Link size is: " + links.size());
		for (int i = 0; i < links.size(); i++) {
			assertThat(getDriver().findElement(By.partialLinkText(links.get(i).get(0))).isDisplayed());

		}
		
	}
}
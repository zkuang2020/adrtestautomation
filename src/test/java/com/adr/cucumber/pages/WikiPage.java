package com.adr.cucumber.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class WikiPage extends PageObject {
	
	@FindBy(id="searchInput")
	protected WebElementFacade searchBarText;
	
	@FindBy(css="#search-form > fieldset > button > i")
	protected WebElementFacade searchButton;
	
	@FindBy(id="firstHeading")
	protected WebElementFacade fruitTitleText;
	

	public void enterFruitNameInSearchBar(String fruit) {
		searchBarText.type(fruit);
		
	}

	public String getPageTitle() {
		// TODO Auto-generated method stub
		return fruitTitleText.getText();
	}

	public void clickSearchBar() {
		searchButton.click();
		
	}

}
